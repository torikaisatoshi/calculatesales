package jp.alhinc.torikai_satoshi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;

public class CaltulateSales {

	public static void main(String[] args) {

		HashMap<String, String> branchMap = new HashMap<>();
		//支店データのマップ キー＝支店番号  マップ＝支店名

		HashMap<String, Long> priceMap = new HashMap<>();
		//金額データのマップ キー＝支店番号 マップ＝金額



		//処理開始
		//支店データの読み込み
		BufferedReader br = null;
		File file1 = new File(args[0], "branch.lst");

		if(!file1.exists()) {
			System.out.println("支店定義ファイルが存在しません");
			return;
		}


		try {
			FileReader fr = new FileReader(file1);
			br = new BufferedReader(fr);

			String line;
			while((line = br.readLine()) != null) {

				String str = line;
				String[] branchList = str.split(",");

				if(branchList.length != 2) {  //支店定義ファイルのカンマの数を確認
					System.out.println("支店定義ファイルのフォーマットが不正です ok");
					return;
				}

				//支店番号が3桁の数字か確認
				if(!branchList[0].matches("[0-9]{3}")) {
					System.out.println("支店定義ファイルのフォーマットが不正です");
					return;
				}

				branchMap.put(branchList[0], branchList[1]);
				priceMap.put(branchList[0], 0L);
			}

		}catch(IOException e) {       //リーダーのエラー
			System.out.println("予期せぬエラーが発生しました ");
			return;

		}finally {
			if(br != null) {
				try {
					br.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました ");
					return;
				}
			}
		}


		//支店読み込む、ここまで

		//ディレクトリ読み込み
		File dir = new File(args[0]);
	    File[] list = dir.listFiles();

	    //連番確認用の変数
	    int oldNum = 0;


	    for(File file : list) {
	    	if(file.getName().matches("[0-9]{8}.rcd")) {
	    		if(file.isFile()) {


			        //連番確認
			        String str = file.getName().replace(".rcd", "");
			        int num = Integer.parseInt(str);

			        if(oldNum != 0) {

						if(num - oldNum != 1) {
							System.out.println("売上ファイル名が連番になっていません");
							return;
						}
			        }
			        oldNum = num;


			        BufferedReader br2 = null;

			        try {
						FileReader fr = new FileReader(file);
						br2 = new BufferedReader(fr);


						String branchCode = br2.readLine();
						String stringPrice = br2.readLine();
						String stringError = br2.readLine();


						//売り上げデータのエラー処理
						if(stringError != null) {  //売り上げファイルが2行か確認
							System.out.println(file.getName() + "のフォーマットが不正です");
							return;
						}


						if(branchMap.get(branchCode) == null) {   //支店コードが支店定義されているか確認
							System.out.println(file.getName() + "の支店コードが不正です");
							return;
						}


						if(stringPrice.length() > 10) {
							System.out.println(file.getName() + "のフォーマットが不正です");
							return;
						}



						//売り上げデータの集計

						try { //金額が数字じゃなければエラー処理
							Long longPrice = Long.parseLong(stringPrice);

							long allPrice = priceMap.get(branchCode);
							priceMap.put(branchCode, allPrice + longPrice);

						}catch(NumberFormatException e) {
							System.out.println(file.getName() + "のフォーマットが不正です");
							return;
						}

						//金額の桁確認
		                if(Long.toString(priceMap.get(branchCode)).length() > 10) {
		                	System.out.println("合計金額が10桁を超えました");
		                	return;
		                }


					} catch (IOException e) {
						System.out.println("予期せぬエラーが発生しました");
						return;
					}finally {
						try {
							br2.close();
						} catch (IOException e) {
							System.out.println("予期せぬエラーが発生しました");
							return;
						}
					}
	    		}
	    	}
	    }


	    //書き出しの処理

	    BufferedWriter bw = null;

	    try {
	    	File file = new File(args[0], "branch.out");
			FileWriter fw = new FileWriter(file);
			bw = new BufferedWriter(fw);


			for (String branchCode : branchMap.keySet()) {
				String message = branchCode + "," + branchMap.get(branchCode) + "," + priceMap.get(branchCode);
				bw.write(message);


			}

		}catch (IOException e) {
			System.out.println("予期せぬエラーが発生しました");
			return;

		}finally {
			if(bw != null) {
				try {
					bw.close();
				}catch(IOException e) {
					System.out.println("予期せぬエラーが発生しました");
					return;
				}
			}
		}
	}
}
